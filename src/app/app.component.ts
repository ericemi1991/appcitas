import { Component } from '@angular/core';
import { Citas } from './models/Cita';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent { 

  listCitas: Citas[] = [];

  listaCitas(cita:Citas):void{
    this.listCitas.push(cita);
  }

  eliminarCita(index:number):void{
    this.listCitas.splice(index, 1);
  }
}
