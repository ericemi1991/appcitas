import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Citas } from 'src/app/models/Cita';

@Component({
  selector: 'listar-citas',
  templateUrl: './listar-citas.component.html',
  styleUrls: ['./listar-citas.component.css']
})
export class ListarCitasComponent implements OnInit {

  @Input() listarCita: Citas[] = []
  @Output() delete = new EventEmitter<number>()

  constructor() { }

  deleteCitas(i: number): void {
    this.delete.emit(i);
  }

  ngOnInit(): void {
  }

}
