import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Citas } from 'src/app/models/Cita';

@Component({
  selector: 'app-crear-citas',
  templateUrl: './crear-citas.component.html',
  styleUrls: ['./crear-citas.component.css']
})
export class CrearCitasComponent implements OnInit {
    
  @Output() getCita = new EventEmitter<Citas>();

    nombre: string;
    fecha: string;
    hora: string;
    sintomas: string;

    formularioIncorrecto: boolean = false;

    //cita: Citas[]=[];

   

  constructor() {
    this.nombre = '';
    this.fecha = '';
    this.hora = '';
    this.sintomas = '';
   }

   addCita(): void {
    if(this.nombre == '' || this.fecha == '' || this.hora == '' || this.sintomas == '' ) {

      this.formularioIncorrecto = true;
      return;
    }else{
      this.formularioIncorrecto = false; 
      //crenado objeto 
      const CITA: Citas = {
        nombre: this.nombre,
        fecha: this.fecha,
        hora: this.hora,
        sintomas: this.sintomas
      }

      this.getCita.emit(CITA);

      this.nombre= '';
      this.fecha = '';
      this.hora = '';
      this.sintomas = '';
    }

   }

  ngOnInit(): void {
  }

}
